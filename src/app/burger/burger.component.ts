import {Component} from '@angular/core';
import {Ingredient} from "../shared/ingredient-model";

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.css']
})
export class BurgerComponent {

  arrayOfStyleClassNames: string[] = [];

  arrayOfIngredients: Ingredient[] = [
    new Ingredient('Meat', 0, 0, 50, 'https://image.shutterstock.com/image-photo/raw-beef-meat-on-cutting-260nw-294503564.jpg'),
    new Ingredient('Cheese', 0, 0, 20, 'https://image.shutterstock.com/image-photo/closeup-american-cheese-slices-on-260nw-401859682.jpg'),
    new Ingredient('Salad', 0, 0, 5, 'https://media.istockphoto.com/photos/lettuce-salad-leaves-isolated-on-white-background-picture-id911794888?s=170667a'),
    new Ingredient('Bacon', 0, 0, 30, 'https://image.shutterstock.com/image-photo/slice-tasty-fried-bacon-on-260nw-1545148244.jpg'),
  ];

  onAddedCount(index: number) {
    this.arrayOfIngredients[index].count++;
    this.getSum(index);
    this.arrayOfStyleClassNames.push(this.arrayOfIngredients[index].ingredient);
  }

  onDeleteCount(index: number) {
    if (this.arrayOfIngredients[index].count > 0) {
      this.arrayOfIngredients[index].count--;
      if (this.arrayOfIngredients[index].count !== 0) {
        this.getSum(index);
      } else {
        this.arrayOfIngredients[index].sum = 0;
      }
    }
    this.onDeleteStyle(index);
  }

  getPrice() {
    return this.arrayOfIngredients.reduce((acc, ingredient) => {
      return acc + ingredient.sum;
    }, 20);
  }

  getSum(index: number) {
    this.arrayOfIngredients[index].sum = this.arrayOfIngredients[index].cost * this.arrayOfIngredients[index].count;
  }

  onDeleteStyle(index:number) {
    const indexDelete = this.arrayOfStyleClassNames.indexOf(this.arrayOfIngredients[index].ingredient);
    if (indexDelete >= 0) {
      this.arrayOfStyleClassNames.splice(indexDelete, 1);
    }
  }
}
