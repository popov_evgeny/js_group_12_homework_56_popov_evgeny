import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BurgerComponent } from './burger/burger.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { ButtonDeleteComponent } from './button-delete/button-delete.component';
import { ButtonAddComponent } from './button-add/button-add.component';
import { IngredientItemComponent } from './ingredient-item/ingredient-item.component';


@NgModule({
  declarations: [
    AppComponent,
    BurgerComponent,
    IngredientComponent,
    ButtonDeleteComponent,
    ButtonAddComponent,
    IngredientItemComponent,

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
