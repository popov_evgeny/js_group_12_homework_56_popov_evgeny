export class Ingredient {
  constructor(
    public ingredient: string,
    public count: number,
    public sum: number,
    public cost: number,
    public link: string,
  ) {}
}

