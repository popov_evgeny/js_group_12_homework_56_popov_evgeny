import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button-add',
  templateUrl: './button-add.component.html',
  styleUrls: ['./button-add.component.css']
})
export class ButtonAddComponent {
  @Input() icon = '';

  @Output() add = new EventEmitter();

  onAdded() {
    this.add.emit()
  }
}
