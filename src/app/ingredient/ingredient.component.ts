import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient-model";

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent {
  @Input() ingredient!: Ingredient;
  @Output() delete = new EventEmitter();
  @Output() add = new EventEmitter();

  onDelete() {
    this.delete.emit()
  }

  onAdded() {
    this.add.emit()
  }
}
